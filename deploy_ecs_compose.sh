#!/bin/bash
#install CLI ECS

usage()
{
	(
	echo "$@"
	echo ""
	echo "Usage: $0 <cluster-name> <repo-name> <access-key> <secret-key>"
	) >&2
	exit 1
}

die()
{
	echo "$0" >&2
	exit 1
}

main()
{


	echo  'Creating ECS Cluster'
	ecs-cli up --keypair sigfox-sandbox --force --capability-iam --size 1 --port 8080  --instance-type m4.xlarge --security-group "sg-65da1e1f" --vpc "vpc-f6b9b094" --subnets "subnet-45280331", "subnet-0e38316c"
	sleep 180
	[ $? -ne 0 ] && die "Cannot create cluster"

	echo  'deploy Microservices  to ECS Cluster'
	 ecs-cli compose --project-name sigfox-sandbox-deploy --file  ./ecs-compose.yml up
	[ $? -ne 0 ] && die "Cannot deploy cluster"

	return 0
}

main "$@"
exit $?
