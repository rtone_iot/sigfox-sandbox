import {Routes} from "@angular/router";
import {GlobalComponent} from "./global/global.component";

export const ROUTES: Routes = [
  {path: 'global', component: GlobalComponent}
]
