import {Component, OnInit} from "@angular/core";
import {ChartModel} from "../model/chart.model";
import {DataService} from "../data.service";
import {DataModel} from "../model/data.model";
import * as moment from "moment";

@Component({
  selector: 'sigfox-global',
  templateUrl: './global.component.html',
  styleUrls: ['./global.component.css']
})
export class GlobalComponent implements OnInit {

  backgroundColors: Array<any> = [{backgroundColor: '#FFE9B9'}, {backgroundColor: '#1FBBFF'},
    {backgroundColor: '#FF83B6'}, {backgroundColor: '#7EFF86'}, {backgroundColor: '#FF5962'},
    {backgroundColor: '#00FF5C'}, {backgroundColor: '#F83CFF'}, {backgroundColor: '#FFAF2B'}];

  barChartLabels: string[] = ['Shoot power'];

  barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  };

  barChartData: Array<ChartModel> = null;

  updateTime: string = '--';

  constructor(private dataService: DataService) {
    dataService.messages
      .filter(chartData => chartData.length >= 1)
      .map(sigfoxData => this.transformSigfoxData(sigfoxData))
      .debounceTime(50)
      .subscribe(chartData => this.barChartData = chartData);
  }

  ngOnInit() {
  }

  private transformSigfoxData(sigfoxData: Array<DataModel>): Array<ChartModel> {
    this.barChartData = null;
    this.updateTime = moment(sigfoxData[0].timestamp).fromNow();
    return sigfoxData.map(data => {
      let chart: ChartModel = {label: data.name, data: [data.data]};
      return chart;
    })
  }

  grabData(): void {
    this.dataService.getMessages()
      .filter(chartData => chartData.length >= 1)
      .map(sigfoxData => this.transformSigfoxData(sigfoxData))
      .debounceTime(150)
      .subscribe(chartData => this.barChartData = chartData);
  }

}
