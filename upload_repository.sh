#!/bin/bash
#install CLI ECS

usage()
{
	(
	echo "$@"
	echo ""
	echo "Usage: $0 <repo-name>  <image-tag> "
	) >&2
	exit 1
}

die()
{
	echo "$0" >&2
	exit 1
}

main()
{
	local repo_name="$1"
	local tag="$2"

	 aws ecr get-login --no-include-email --region eu-west-1 | sh -
	[ $? -ne 0 ] && die "Cannot login to registry"

	docker tag "rtone/$repo_name:$tag" "792475852086.dkr.ecr.eu-west-1.amazonaws.com/rtone/$repo_name:$tag"
	[ $? -ne 0 ] && die "Cannot tag image"

	echo "Pushing image to Rtone ECR $repo_name"
	docker push "792475852086.dkr.ecr.eu-west-1.amazonaws.com/rtone/$repo_name:$tag"
	[ $? -ne 0 ] && die "Cannot push image"
	return 0
}

main "$@"
exit $?
