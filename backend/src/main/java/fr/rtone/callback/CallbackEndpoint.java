package fr.rtone.callback;

import fr.rtone.domain.SigfoxData;
import fr.rtone.dto.CallbackDataDTO;
import fr.rtone.dto.SigfoxDataDTO;
import fr.rtone.mapper.SigfoxDataMapper;
import fr.rtone.repository.SigfoxDataRepository;
import fr.rtone.websocket.SigfoxDataHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * @Author: Hani
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class CallbackEndpoint {

    @Autowired
    SigfoxDataRepository repository;
    @Autowired
    SigfoxDataMapper sigfoxDataMapper;
    @Autowired
    SigfoxDataHandler dataHandler;

    @Value("${sandbox.id}")
    String name;

    @GetMapping("/ping")
    public ResponseEntity<String> ping() {
        return ResponseEntity.ok("Works successfully");
    }

    @GetMapping("/profile")
    public ResponseEntity<String> getProfile() {
        return ResponseEntity.ok(name);
    }

    @GetMapping("/messages")
    public List<SigfoxDataDTO> getData() {
        List<SigfoxData> sigfoxData = repository.findAll(new Sort(Sort.Direction.DESC, "timestamp"))
                .stream()
                .filter(distinctByKey(SigfoxData::getDevice)).collect(Collectors.toList());

        return sigfoxDataMapper.dataToDataDTOs(sigfoxData);
    }


    @GetMapping("/device/messages")
    public List<SigfoxDataDTO> getDeviceData() {

        List<SigfoxData> sigfoxData = repository.findByName(name);

        dataHandler.pushSigfoxData(sigfoxDataMapper.dataToDataDTOs(sigfoxData));

        return sigfoxDataMapper.dataToDataDTOs(sigfoxData);
    }


    @PostMapping("/uplink")
    public String uplinkEndpoint(CallbackDataDTO callbackData) {

        SigfoxData sigfoxData = new SigfoxData();

        // Device Id
        sigfoxData.setDevice(callbackData.getDevice());

        // Device name to show
        sigfoxData.setName(name);


        // formulation to compute power of the shoot
        short accX = hexStringToLong(callbackData.getData().substring(2, 4).concat(callbackData.getData().substring(0, 2)));
        short accY = hexStringToLong(callbackData.getData().substring(6, 8).concat(callbackData.getData().substring(4, 6)));
        short accZ = hexStringToLong(callbackData.getData().substring(10, 12).concat(callbackData.getData().substring(8, 10)));

        long value;

        /* ****************************************************************
        * TODO: Implement your function here and assign result to @value
        * ****************************************************************/

        value = accX + accY + accZ;

        //Data
        sigfoxData.setData(String.valueOf(value));
        // Timestamp
        sigfoxData.setTimestamp(callbackData.getTime() * 1000);

        repository.save(sigfoxData);
        repository.flush();

        List<SigfoxData> sigfoxDataList = repository.findByName(name);

        dataHandler.pushSigfoxData(sigfoxDataMapper.dataToDataDTOs(sigfoxDataList));

        System.out.println("Passed by here " + callbackData.getData());

        Long downlink = 102030405060708L;


        /* ****************************************************************
        * TODO: Implement your function here and assign result to @downlink
        * ****************************************************************/


        String payload = "{ \"" + callbackData.getDevice() + "\" : {\"downlinkData\" : \"" + String.valueOf(downlink) + "\" }}";

        return payload;
    }

    private static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {
        Map<Object, Boolean> map = new ConcurrentHashMap<>();
        return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    private static short hexStringToLong(String hexValue) {
        return (short) Integer.parseInt(hexValue, 16);
    }

}
