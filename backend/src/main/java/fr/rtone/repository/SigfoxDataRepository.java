package fr.rtone.repository;

import fr.rtone.domain.SigfoxData;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @Author: Hani
 */
public interface SigfoxDataRepository extends JpaRepository<SigfoxData, Long> {

    List<SigfoxData> findByName(String name);

}
