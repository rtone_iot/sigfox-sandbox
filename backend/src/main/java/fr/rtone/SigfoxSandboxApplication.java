package fr.rtone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan
public class SigfoxSandboxApplication {

	public static void main(String[] args) {
		SpringApplication.run(SigfoxSandboxApplication.class, args);
	}
}
