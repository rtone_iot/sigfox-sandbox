package fr.rtone.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Author: Hani
 */
@Controller
@CrossOrigin(origins = "*")
public class RedirectController {


    @RequestMapping({"global"})
    public String index() {
        String forward = "forward:/";
        return forward;
    }

}
