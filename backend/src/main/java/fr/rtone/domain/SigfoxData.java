package fr.rtone.domain;

import javax.persistence.*;

/**
 * @Author: Hani
 */
@Entity
@Table(name = "sigfoxdata")
public class SigfoxData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String device;
    private Long timestamp;
    private String data;
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SigfoxData that = (SigfoxData) o;

        if (!device.equals(that.device)) return false;
        if (!timestamp.equals(that.timestamp)) return false;
        return data.equals(that.data);
    }

    @Override
    public int hashCode() {
        int result = device.hashCode();
        result = 31 * result + timestamp.hashCode();
        result = 31 * result + data.hashCode();
        return result;
    }
}
