package fr.rtone.mapper;

import fr.rtone.domain.SigfoxData;
import fr.rtone.dto.SigfoxDataDTO;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @Author: Hani
 */
@Mapper(componentModel = "spring")
public interface SigfoxDataMapper {

    SigfoxDataDTO dataToDataDTO(SigfoxData data);

    SigfoxData dataDTOToData(SigfoxDataDTO data);

    List<SigfoxDataDTO> dataToDataDTOs(List<SigfoxData> datas);

    List<SigfoxData> dataDTOsToData(List<SigfoxDataDTO> datas);
}
